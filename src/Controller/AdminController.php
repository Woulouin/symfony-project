<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Sujet;
use App\Entity\Category;
use App\Form\EditUserType;
use App\Repository\UserRepository;
use App\Repository\SujetRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
     * @Route("/admin", name="admin_")
     */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/users", name="users")
     */
    public function usersList(UserRepository $users) {
        return $this->render("admin/users.html.twig", [
            'users' => $users->findAll()
        ]);
    }

    /**
     * @Route("/user/edit/{id}", name="user_edit")
     */
    public function editUser(User $user, Request $request, EntityManagerInterface $manager) {
        $form = $this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/edituser.html.twig', [
            'userForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/sujets", name="sujets")
     */
    public function sujetsList(SujetRepository $sujet) {
        return $this->render("admin/sujets.html.twig", [
            'sujets' => $sujet->findAll()
        ]);
    }

    /**
     * @Route("/delete/sujet/{id}", name="delete_sujet")
     */
    public function deleteSujet(Sujet $sujet, EntityManagerInterface $manager) {
        
        $manager->remove($sujet);
        $manager->flush();

        $this->addFlash('message', 'Sujet supprimé avec succès');
        return $this->redirectToRoute('admin_sujets');
    }

    /**
     * @Route("/categories", name="categories")
     */
    public function categoriesList(CategoryRepository $categorie) {
        return $this->render("admin/categories.html.twig", [
            'categories' => $categorie->findAll()
        ]);
    }

    /**
     * @Route("/delete/categorie/{id}", name="delete_categorie")
     */
    public function deleteCategorie(Category $categorie, EntityManagerInterface $manager) {
        
        $manager->remove($categorie);
        $manager->flush();

        $this->addFlash('message', 'Catégorie supprimé avec succès');
        return $this->redirectToRoute('admin_categorie');
    }

    /**
     * @Route("/category/new", name="category_create")
     */
    public function formCategory(Category $category = null, Request $request, EntityManagerInterface $manager) {
        
        $category = new Category();

        $form = $this->createFormBuilder($category)
                     ->add('title')
                     ->add('description')
                     ->getForm();
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($category);
            $manager->flush();

            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('admin/createCategory.html.twig', [
            'formCategory' => $form->createView()
        ]);
    }
}
