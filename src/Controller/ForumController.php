<?php

namespace App\Controller;

use App\Entity\Sujet;
use App\Entity\Report;
use App\Entity\Comment;
use App\Entity\Category;
use App\Entity\PostLike;
use App\Form\CommentType;
use App\Entity\PostDislike;
use App\Repository\SujetRepository;
use App\Repository\ReportRepository;
use App\Repository\PostLikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PostDislikeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ForumController extends AbstractController
{
    /**
     * @Route("/forum", name="forum")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Sujet::class);

        $sujets = $repo->findAll();

        return $this->render('forum/index.html.twig', [
            'controller_name' => 'ForumController',
            'sujets' => $sujets
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home() {
        return $this->render('forum/home.html.twig');
    }

    /**
     * @Route("/forum/new", name="forum_create")
     * @Route("/forum/{id}/edit", name="forum_edit")
     */
    public function form(Sujet $sujet = null, Request $request, EntityManagerInterface $manager) {
        if (!$sujet) {
            $sujet = new Sujet();
        }

        $form = $this->createFormBuilder($sujet)
                     ->add('title')
                     ->add('category', EntityType::class, [
                         'class' => Category::class,
                         'choice_label' => 'title'
                     ])
                     ->add('content')
                     ->getForm();
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$sujet->getId()) {
                $sujet->setCreatedAt(new \DateTime());
            }

            $manager->persist($sujet);
            $manager->flush();

            return $this->redirectToRoute('forum_show', ['id' =>$sujet->getId()]);
        }

        return $this->render('forum/create.html.twig', [
            'formSujet' => $form->createView(),
            'editMode' => $sujet->getId() !== null
        ]);
    }

    /**
     * @Route("/forum/{id}", name="forum_show")
     */
    public function show($id, Request $request, EntityManagerInterface $manager) {
        
        $repo = $this->getDoctrine()->getRepository(Sujet::class);
        $sujet = $repo->find($id);
        $user = $this->getUser()->getUsername();

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $comment->setCreatedAt(new \Datetime())
                    ->setSujet($sujet)
                    ->setAuthor($user);
            
            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('forum_show', ['id' => $sujet->getId()]);
        }

        return $this->render('forum/show.html.twig', [
            'sujet' => $sujet,
            'commentForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/sujet/{id}/like", name="sujet_like")
     */
    public function like(Sujet $sujet, EntityManagerInterface $manager, PostLikeRepository $likeRepo): Response {
        $user = $this->getUser();

        if(!$user) return $this->json([
            'code' => 403,
            'message' => "Non autorisé"
        ], 403);

        if($sujet->isLikedByUser($user)) {
            $like = $likeRepo->findOneBy([
                'post' => $sujet,
                'user' => $user
            ]);

            $manager->remove($like);
            $manager->flush();

            return $this->json([
                'code' => 200,
                'message' => "Like bien supprimé",
                'likes' => $likeRepo->count(['post' => $sujet])
            ], 200);
        }
        
        $like = new PostLike();
        $like->setPost($sujet)
             ->setUser($user);

        $manager->persist($like);
        $manager->flush();

        return $this->json([
            'code' => 200,
            'message' => 'Like bien ajouté',
            'likes' => $likeRepo->count(['post' => $sujet])
        ], 200);
    }

    /**
     * @Route("/sujet/{id}/dislike", name="sujet_dislike")
     */
    public function dislike(Sujet $sujet, EntityManagerInterface $manager, PostDislikeRepository $dislikeRepo): Response {
        $user = $this->getUser();

        if(!$user) return $this->json([
            'code' => 403,
            'message' => "Non autorisé"
        ], 403);

        if($sujet->isDislikedByUser($user)) {
            $dislike = $dislikeRepo->findOneBy([
                'post' => $sujet,
                'user' => $user
            ]);

            $manager->remove($dislike);
            $manager->flush();

            return $this->json([
                'code' => 200,
                'message' => "Dislike bien supprimé",
                'dislikes' => $dislikeRepo->count(['post' => $sujet])
            ], 200);
        }
        
        $dislike = new PostDislike();
        $dislike->setPost($sujet)
                ->setUser($user);

        $manager->persist($dislike);
        $manager->flush();

        return $this->json([
            'code' => 200,
            'message' => 'Dislike bien ajouté',
            'dislikes' => $dislikeRepo->count(['post' => $sujet])
        ], 200);
    }

    /**
     * @Route("/sujet/{id}/report", name="sujet_report")
     */
    public function report(Sujet $sujet, EntityManagerInterface $manager, ReportRepository $reportRepo): Response {
        $user = $this->getUser();

        if(!$user) return $this->json([
            'code' => 403,
            'message' => "Non autorisé"
        ], 403);

        if($sujet->isReportedByUser($user)) {
            $report = $reportRepo->findOneBy([
                'post' => $sujet,
                'user' => $user
            ]);

            $manager->remove($report);
            $manager->flush();

            return $this->json([
                'code' => 200,
                'message' => "Report bien supprimé",
                'reports' => $reportRepo->count(['post' => $sujet])
            ], 200);
        }
        
        $report = new Report();
        $report->setPost($sujet)
             ->setUser($user);

        $manager->persist($report);
        $manager->flush();

        return $this->json([
            'code' => 200,
            'message' => 'Report bien ajouté',
            'reports' => $reportRepo->count(['post' => $sujet])
        ], 200);
    }
}
